#!/bin/sh

TAG="$1"

if [ -z "$TAG" ]; then
    echo "TAG argument required"
    exit 1
fi

git tag "$TAG"
git push origin "$TAG"