package main

import (
	"bitbucket.org/atlassian/sink/logger"
	"bitbucket.org/atlassian/sink/metrics"
)

func main() {

	// create logger with default info level
	l := logger.NewLogger()

	// this shouldn't be logged
	l.WithFields("you", "can't").Debug("see me")

	// this should be logged
	l.WithFields("example_field_name", "example_field_value").Info("example message %d", 1)

	// create metrics reporter
	m, err := metrics.New("localhost:8080", "example_app")
	if err != nil {
		l.Warn("can't reach localhost:8080, metrics.NoOpMetrics returned instead")
	}

	// no op
	m.Count("example_key", "foo", "bar")
}
